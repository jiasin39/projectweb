import base64
import io
import os
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from torchvision import models
from torchvision import transforms
from PIL import Image
from django.shortcuts import render
from django.conf import settings

# from .forms import ImageUploadForm

from django.shortcuts import render
from django.http import JsonResponse
from .forms import ImageFileUploadForm

# Create your views here.

def image_upload_method(request):
    if request.method == "POST":
        form = ImageFileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            print(form.cleaned_data)
            image = form.cleaned_data.get('選擇檔案')
            print(image)
            name = form.cleaned_data.get('請輸入員工編號')
            print(name)

            return JsonResponse({'error':False, 'message': 'Uploaded Successfully.'})
        else:
            return JsonResponse({'error':True, 'errors': form.errors})
    else:
        form = ImageFileUploadForm()
        return render(request, 'upload_ui.html', {'form': form})

###
#相對路徑搜尋
absolutepath = os.path.abspath(__file__)
fileDirectory = os.path.dirname(absolutepath)
#Path of parent directory
parentDirectory = os.path.dirname(fileDirectory)
#Navigate to Strings directory
PATH = os.path.join(parentDirectory, 'Mingmodel_resnet18.pth' )   


# train_model_name='旻家' #佳欣 旻家 明賢 榮昇 珮如 仲容
# PATH='旻家model_resnet18' 
# PATH = 'C:\\Users\\user\\desktop\\handwriting_recognition_project\\旻家model_resnet18.pth'
# classes = ('false', 'true') # 分類的類別
classes = ('F仲容', 'F佳欣', 'F旻家', 'F明賢', 'F榮昇', 'F珮如', 'others', 'photos', '仲容', '佳欣', '旻家', '明賢', '榮昇', '珮如') # 分類的類別
num_classes = len(classes)
class ResidualBlock(nn.Module):
    def __init__(self, inchannel, outchannel, stride=1):
        super(ResidualBlock, self).__init__()
        self.left = nn.Sequential(
            nn.Conv2d(inchannel, outchannel, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(outchannel),
            nn.ReLU(inplace=True),
            nn.Conv2d(outchannel, outchannel, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(outchannel)
        )
        self.shortcut = nn.Sequential()
        if stride != 1 or inchannel != outchannel:
            self.shortcut = nn.Sequential(
                nn.Conv2d(inchannel, outchannel, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(outchannel)
            )

    def forward(self, x):
        out = self.left(x)
        out += self.shortcut(x)
        out = F.relu(out)
        return out

class ResNet(nn.Module):
    def __init__(self, ResidualBlock, num_classes=10):
        super(ResNet, self).__init__()
        self.inchannel = 64
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(),
        )
        self.layer1 = self.make_layer(ResidualBlock, 64,  2, stride=1)
        self.layer2 = self.make_layer(ResidualBlock, 128, 2, stride=2)
        self.layer3 = self.make_layer(ResidualBlock, 256, 2, stride=2)
        self.layer4 = self.make_layer(ResidualBlock, 512, 2, stride=2)
        self.fc = nn.Linear(8192, num_classes) # 原512改8192

    def make_layer(self, block, channels, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)   #strides=[1,1]
        layers = []
        for stride in strides:
            layers.append(block(self.inchannel, channels, stride))
            self.inchannel = channels
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out

def ResNet18():
  return ResNet(ResidualBlock, num_classes=num_classes)

###
model = ResNet18()
model.load_state_dict(torch.load(PATH,map_location='cpu'))
model.eval()
###
# img = cv2.imread(f'{path_c}/{i}')
# my_transforms = transforms.Compose([transforms.ToTensor(), transforms.Resize((128,128)), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
# img_tensor = my_transforms(img)
# test1 = img_tensor.unsqueeze(0)
# outputs = model(test1)
# _, predicted = torch.max(outputs.data, 1)
###
# json_path = os.path.join(settings.STATIC_ROOT, "imagenet_class_index.json")
# imagenet_mapping = json.load(open(json_path))


def transform_image(image_bytes):

    my_transforms = transforms.Compose([transforms.Resize((128,128)),
                                        transforms.ToTensor(),
                                        transforms.Normalize(
                                            [0.5, 0.5, 0.5],
                                            [0.5, 0.5, 0.5])])
    img = Image.open(io.BytesIO(image_bytes))
    return my_transforms(img).unsqueeze(0)
    # image = Image.open(io.BytesIO(image_bytes))
    # return my_transforms(image).unsqueeze(0)

def get_prediction(image_bytes):
    # tensor = transform_image(image_bytes)
    # outputs = model.forward(tensor)
    # _, y_hat = outputs.max(1)
    # predicted_idx = str(y_hat.item())
    # class_name, human_label = imagenet_mapping[predicted_idx]
    img_tensor = transform_image(image_bytes)
    outputs = model(img_tensor)
    _, predicted = torch.max(outputs.data, 1)
    if str(classes[predicted[0]])=='F仲容':
        ans='F仲容'
        return ans
    elif str(classes[predicted[0]])=='F珮如':
        ans='F珮如'
        return ans
    elif str(classes[predicted[0]])=='F旻家':
        ans='F旻家'
        return ans
    elif str(classes[predicted[0]])=='F佳欣':
        ans='F佳欣'
        return ans
    elif str(classes[predicted[0]])=='F榮昇':
        ans='F榮昇'
        return ans
    elif str(classes[predicted[0]])=='F明賢':
        ans='F明賢'
        return ans
    elif str(classes[predicted[0]])=='仲容':
        ans='仲容'
        return ans
    elif str(classes[predicted[0]])=='旻家':
        ans='旻家'
        return ans
    elif str(classes[predicted[0]])=='珮如':
        ans='珮如'
        return ans
    elif str(classes[predicted[0]])=='榮昇':
        ans='榮昇'
        return ans
    elif str(classes[predicted[0]])=='佳欣':
        ans='佳欣'
        return ans
    elif str(classes[predicted[0]])=='明賢':
        ans='明賢'
        return ans
    elif str(classes[predicted[0]])=='ohters':
        ans='others'
        return ans
    elif str(classes[predicted[0]])=='photos':
        ans='photos'
        return ans
  


def index(request):
    image_uri = None
    predicted_label = None

    if request.method == 'POST':
        form = ImageFileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            # passing the image as base64 string to avoid storing it to DB or filesystem
            image = form.cleaned_data['選擇檔案']
            image_bytes = image.file.read()
            encoded_img = base64.b64encode(image_bytes).decode('ascii')
            image_uri = 'data:%s;base64,%s' % ('image/jpeg', encoded_img)

            # get predicted label
            try:
                predicted_label = get_prediction(image_bytes)
            except RuntimeError as re:
                print(re)
                # predicted_label = "Prediction Error"
            print(predicted_label)
            if predicted_label == 'F仲容':
                return JsonResponse({'error':False, 'message': '非本人，請重新上傳簽名案!'})
            elif predicted_label == 'F珮如':
                return JsonResponse({'error':False, 'message': '非本人，請重新上傳簽名案!'})
            elif predicted_label == 'F旻家':
                return JsonResponse({'error':False, 'message': '非本人，請重新上傳簽名案!'}) 
            elif predicted_label == 'F佳欣':
                return JsonResponse({'error':False, 'message': '非本人，請重新上傳簽名案!'})  
            elif predicted_label == 'F明賢':
                return JsonResponse({'error':False, 'message': '非本人，請重新上傳簽名案!'})
            elif predicted_label == 'F榮昇':
                return JsonResponse({'error':False, 'message': '非本人，請重新上傳簽名案!'})
            elif predicted_label == 'others':
                return JsonResponse({'error':False, 'message': '非本人，若有錯誤請重新上傳檔案!'})
            elif predicted_label == 'photos':
                return JsonResponse({'error':False, 'message': '非本人，若有錯誤請重新上傳檔案!'}) 
            elif predicted_label == '榮昇':
                return JsonResponse({'error':False, 'message': '簽到確認!謝謝!'})
            elif predicted_label == '仲容':
                return JsonResponse({'error':False, 'message': '簽到確認!謝謝!'})  
            elif predicted_label == '珮如':
                return JsonResponse({'error':False, 'message': '簽到確認!謝謝!'})  
            elif predicted_label == '佳欣':
                return JsonResponse({'error':False, 'message': '簽到確認!謝謝!'})  
            elif predicted_label == '旻家':
                return JsonResponse({'error':False, 'message': '簽到確認!謝謝!'})  
            elif predicted_label == '明賢':
                return JsonResponse({'error':False, 'message': '簽到確認!謝謝!'}) 
            else:
                return JsonResponse({'error':False, 'message': '非本人，若有錯誤請重新上傳檔案!'})
    else:
        form = ImageFileUploadForm()

    context = {
        'form': form,
        'image_uri': image_uri,
        'predicted_label': predicted_label,
    }
    return render(request, 'upload_ui.html', context)